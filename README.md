# AWS Basics
### 22 - 23 April 2021 (*remote course*)

# Links
* [Installation instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)  (*not applicable for classroom course*)
* [Course Details](https://www.ribomation.se/courses/cloud-computing/aws-basics)


# AWS Account
In order to interact with AWS and perform the exercises, you need
to sign-up for an AWS account. Please, do this before the course
starts. You probably need to provide a credit/debit card to cover
any excess spending. 
* [Sign-Up for AWS](https://portal.aws.amazon.com/billing/signup#/start)

The exercises in the course are not intended to cost anything. In
most cases, you will be using the AWS Free Tier.

Just remember to shut down any running services, that are charged
per hour, such as EC2 servers and RDS databases.


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a sub-directory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/aws-course/my-solutions
    cd ~/aws-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/aws-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
