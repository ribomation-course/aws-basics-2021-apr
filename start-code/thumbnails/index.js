const AWS   = require('aws-sdk');
const S3    = new AWS.S3();
const SHARP = require('sharp');

const thumbNailDefs = [
    {keySuffix: 'thumb', width: 64},
    {keySuffix: 'small', width: 256},
    {keySuffix: 'medium', width: 512},
    {keySuffix: 'large', width: 1024}
];

function getImageType(filename) {
    const extMatch = filename.match(/\.([^.]*)$/);
    if (!extMatch) {
        throw new Error('No file ext');
    }
    const ext = extMatch[1].toLowerCase();
    if (!['jpg', 'png', 'gif', 'jpeg'].includes(ext)) {
        throw new Error('Unsupported image type: ' + ext);
    }
    const basename = filename.substr(0, filename.length - ext.length - 1);
    return [ext, basename];
}

async function resizeAnUpload(srcObject, defs, dstBucket, imgType, name) {
    const imgData = await SHARP(srcObject.Body)
        .resize({width: defs.width})
        .toFormat(imgType)
        .toBuffer();

    const dstKey = `${name}-${defs.keySuffix}.${imgType}`;
    await S3.putObject({
        Bucket: dstBucket,
        Key: dstKey,
        Body: imgData,
        ContentType: srcObject.ContentType
    }).promise();

    console.log(`Resized image to ${dstBucket}/${dstKey}, with width=${defs.width}px`);
}

exports.handler = async (ev, ctx) => {
    console.log('S3 Event:', JSON.stringify(ev, null, 4));
    const evS3 = ev.Records[0].s3;

    const srcBucket = evS3.bucket.name;
    const dstBucket = srcBucket + '-thumbnails';
    if (srcBucket === dstBucket) {
        throw new Error('Same buckets !!');
    }

    const srcKey = decodeURIComponent(evS3.object.key.replace(/\+/g, ' '));
    //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/decodeURIComponent

    try {
        const srcObject = await S3.getObject({
            Bucket: srcBucket,
            Key: srcKey
        }).promise();
        console.log('Downloaded image %s/%s, type=%s, size=%d, date=%s',
            srcBucket, srcKey, srcObject.ContentType, srcObject.ContentLength, srcObject.LastModified);

        const [imgType, name] = getImageType(srcKey);
        for (let defs of thumbNailDefs) {
            await resizeAnUpload(srcObject, defs, dstBucket, imgType, name);
        }

        console.log('done');
    } catch (err) {
        console.error(`Failed to resize ${srcBucket}/${srcKey}: ${err}`);
    }

    return 'done';
};
