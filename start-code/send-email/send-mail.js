const nodemailer = require("nodemailer");

const smtp = {
    host: 'email-smtp.<region>.amazonaws.com',
    port: 465,
    secure: true,
    auth: {
        user: '<smtp user name>',
        pass: '<smtp pass word>'
    }
};

const email = {
    from: '<validated email>',
    to: '<validated email>',
    subject: 'Test AWS SES SMTP',
    text: 'Hi there from AWS SMTP',
    html: `<h1>Test Mail</h1>
           <p>
             Hi from <code style="color:orange;">AWS SMTP</code>
           </p>`
};

async function run() {
    const transporter = nodemailer.createTransport(smtp);
    const result      = await transporter.sendMail(email);
    console.log('Email sent: %s', result.messageId);
}

run().catch(console.error);
